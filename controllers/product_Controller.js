const Product = require("../models/Product_models");
const User = require("../models/User_models");

// CREATE PRODUCT (ADMIN ONLY)...................................................
module.exports.addProduct = (reqBody) => {
  let newProduct = new Product({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  });

  return newProduct
    .save()
    .then((product) => true)
    .catch((err) => false);
};

// RETRIEVE ALL COURSES ..............................................................
module.exports.getAllProducts = () => {
  return Product.find({})
    .then((result) => result)
    .catch((err) => err);
};

// RETRIEVE ALL "ACTIVE" PRODUCTS ............................................
module.exports.getAllActiveProducts = () => {
  return Product.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

// RETRIEVE "SINGLE" PRODUCTS ...........................................
module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams.productId)
    .then((result) => {
      return result;
    })
    .catch((err) => err);
};

// UPDATE PRODUCT INFORMATION (ADMIN ONLY) .........................................
//redo => "true" for line52 soon....
module.exports.updateProduct = (reqParams, reqBody) => {
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then((product) => {
      return `Product has been successfully updated. :\n\n-----${reqBody.name}\n-----${reqBody.description}\n-----${reqBody.price}`;
    })
    .catch((err) => err);
};

// ARCHIVE PRODUCT (ADMIN ONLY) .........................................
module.exports.archiveProduct = (reqParams) => {
  let archiveActiveField = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(reqParams.productId, archiveActiveField)
    .then((course) => true)
    .catch((err) => err);
};

// UNARCHIVE PRODUCT (ADMIN ONLY) .........................................
module.exports.unarchiveProduct = (reqParams) => {
  let archiveActiveField = {
    isActive: true,
  };

  return Product.findByIdAndUpdate(reqParams.productId, archiveActiveField)
    .then((course) => true)
    .catch((err) => err);
};

// NON-ADMIN USER CHECKOUT (CREATE ORDER)_________________________________________________________________
module.exports.createOrder = async (data) => {
  let isUserCreated = await User.findById(data.userId).then((user) => {
    user.orderedProduct[products].push({ productId: data.productId });

    return user
      .save()
      .then((user) => true)
      .catch((err) => false);
  });

  let isProductCreated = await Product.findById(data.productId).then(
    (product) => {
      product.userOrders.push({ userId: data.userId });

      return product
        .save()
        .then((product) => true)
        .catch((err) => false);
    }
  );

  if (isUserCreated && isProductCreated) {
    return true;
  } else {
    return false;
  }
};
